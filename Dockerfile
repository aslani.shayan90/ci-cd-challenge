#I used the example from the following link with some changes: https://www.nginx.com/blog/deploying-nginx-nginx-plus-docker/
# we can use the following docker file but I perefer to write less code: https://github.com/nginxinc/docker-nginx/blob/d4a47bc6602d3a1412dad48a8513b83805605ef3/mainline/alpine/Dockerfile
# I use nginx:alpine to reduce the size of image and also to reduce the security risks
FROM nginx:alpine
MAINTAINER Shayan SariAslan "aslani.shayan90@gmail.com"
# to update the packages and Resolving some security issues, I run the following code:
RUN apk update && apk upgrade
# copy config file and index.html file to show shows a page with this content: "Hello World Yougov!"
RUN rm /usr/share/nginx/html/index.html && mkdir -p /var/www/mytest && rm /etc/nginx/conf.d/default.conf
COPY default.conf /etc/nginx/conf.d/default.conf
COPY index.html /var/www/mytest/index.html
VOLUME /usr/share/nginx/html
VOLUME /etc/nginx

